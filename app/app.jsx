import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import Main from '../components/Main';

// import './styles/main.scss';
// import routes from './config/routes.jsx';
import store  from '../store/store';

ReactDOM.render(
    <Provider store={store}>
        <Main />
    </Provider>,
    document.getElementById('app')
);
