export default [
    {
        login: 'q',
        password: 'q',
        role: 'host'
    },
    {
        login: 'w',
        password: 'w',
        role: 'guest'
    },
    {
        login: 'e',
        password: 'e',
        role: 'guest'
    }
];
