import { createStore, combineReducers } from 'redux';
import mainReducer from './reducers/mainReducer';

const combinedReducers = combineReducers({
    main: mainReducer
});

const store = createStore(
  combinedReducers
);

export default store;
