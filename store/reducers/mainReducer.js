const initialState = {
    display: {
        where: 'main',
        data: null
    },
    user: {
        // TODO: false
        loggedIn: false,
        role: null,
        login: null
    },
    harmonogram: [
        {
            id: 384763,
            title: 'Sesja zdjęciowa',
            date: '11.08.2018 15:00'
        },
        {
            id: 46071,
            title: 'Ślub',
            date: '20.08.2018 16:00'
        }
    ],
    guests: [
        'w'
    ]
};

export default function (state = initialState, action) {
    const { type, payload } = action;
    let index;
    switch (type) {
        case 'LOGIN':
            return Object.assign({}, state, {
                user: {
                    loggedIn: true,
                    role: payload.role,
                    login: payload.login
                }
            });
        case 'LOGOUT':
            return Object.assign({}, state, {
                user: {
                    loggedIn: false,
                    role: null,
                    login: null
                }
            });
        case 'CONFIRM_COMING':
            let { guests, user } = state;
            index = guests.indexOf(user.login);
            if (payload) {
                guests.push(user.login);
            } else {
                if (index > -1) {
                    guests.splice(index, 1);
                }
            }
            return Object.assign({}, state, {
                guests
            });
        case 'CHANGE_DISPLAY':
            return Object.assign({}, state, {
                display: {
                    where: payload.where,
                    data: payload.data
                }
            });
        case 'ADD_EDIT_EVENT':
            let harmonogram = state.harmonogram;
            index = harmonogram.findIndex(e => e.id === payload.id);
            if (index >= 0) {
                harmonogram[index] = payload;
            } else {
                harmonogram.push(payload);
            }
            return Object.assign({}, state, {
                harmonogram
            });
        default:
            return state
    }
}
