import React from 'react';
import { connect } from 'react-redux';

import Users from '../store/Users';

class GuestList extends React.Component {
    render() {
        const { main, confirmComing } = this.props;
        const { guests, user } = main;
        debugger;
        return (
            <div>
                <table className="table">
                    <thead>
                        <tr>
                            <th>Gość</th>
                            <th>Potwierdzono</th>
                        </tr>
                    </thead>
                    <tbody>
                        {Users.map((e, i) =>
                            e.role !== 'host' &&
                            <tr key={i}>
                                <td>{e.login}</td>
                                <td>{guests.includes(e.login) ? 'Tak' : 'Nie'}</td>
                            </tr>)}
                    </tbody>
                </table>
                {user.role !== 'host' ?
                    guests.includes(user.login) ?
                        <button className="btn btn-danger" onClick={() => confirmComing(false)}>Anuluj potwierdzenie</button> :
                        <button className="btn btn-primary" onClick={() => confirmComing(true)}>Potwierdź</button>
                    : null
                }
            </div>
        )
    }
}

const mapDispatchToProps = (dispatch) => ({
    confirmComing: (payload) => {
        dispatch({type: 'CONFIRM_COMING', payload});
    }
});

export default connect(
    null,
    mapDispatchToProps
)(GuestList);
