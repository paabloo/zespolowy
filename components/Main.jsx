import React from 'react';
import { connect } from 'react-redux';

import LogIn from './LogIn';
import Page from './Page';

class Main extends React.Component {
    render() {
        const { state } = this.props;
        const { user, harmonogram } = state.main;
        const logIn = <LogIn user={user} />;
        const page = <Page state={state} />;
        console.log(user);
        return (
            <div style={{paddingTop: 40}}>{user.loggedIn ? page : logIn}</div>
        )
    }
}

const mapStateToProps = (state) => ({
    state
});

export default connect(
    mapStateToProps
)(Main);
