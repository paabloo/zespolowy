import React from 'react';
import { connect } from 'react-redux';
import Users from '../store/Users';

class LogIn extends React.Component {
    constructor() {
        super();

        this.state = {
            login: '',
            password: ''
        }

        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit(el) {
        el.preventDefault();
        const { login, password } = this.state;
        Users.forEach(e => {
            if (e.login === login && e.password === password) {
                this.props.logIn(e);
            }
        });
    }

    render() {
        const { user } = this.props;
        return (
            <div className="container">
                <h1>Logowanie</h1>
                <div className="well">
                    <form onSubmit={this.handleSubmit}>
                        <div className="form-group">
                            <label htmlFor="login">Login</label>
                            <input type="text" className="form-control" id="login" placeholder="Login" value={this.state.login} onChange={(e) => this.setState({login: e.target.value})} />
                        </div>
                        <div className="form-group">
                            <label htmlFor="pass">Hasło</label>
                            <input type="password" className="form-control" id="pass" placeholder="******" value={this.state.password} onChange={(e) => this.setState({password: e.target.value})} />
                        </div>
                        <button type="submit" className="btn btn-primary">Zaloguj</button>
                    </form>
                </div>
            </div>
        )
    }
}

const mapDispatchToProps = (dispatch) => ({
    logIn: (payload) => {
        dispatch({type: 'LOGIN', payload});
    }
});

export default connect(
    null,
    mapDispatchToProps
)(LogIn);
