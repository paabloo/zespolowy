import React from 'react';
import { connect } from 'react-redux';

import Harmonogram from './Harmonogram';
import GuestList from './GuestList';
import EventForm from './EventForm';

class Page extends React.Component {
    render() {
        const { logIn } = this.props;
        const { main } = this.props.state;
        const { harmonogram, user, guests } = main;
        console.log(main);
        let element, title;
        switch (main.display.where) {
            case 'main':
                title = "Strona główna";
                element = <div>
                    <Harmonogram harmonogram={harmonogram} user={user} />
                    <GuestList main={main} />
                </div>;
                break;
            case 'edit':
                title = "Dodaj/edytuj wydarzenie";
                element = <div><EventForm /></div>;
                break;
        }
        return (
            <div className="container">
                <h1>{title}</h1>
                <button className="btn btn-warning" onClick={logIn}>Wyloguj</button>
                {element}
            </div>
        )
    }
}

const mapDispatchToProps = (dispatch) => ({
    logIn: () => {
        dispatch({type: 'LOGOUT'});
        dispatch({type: 'CHANGE_DISPLAY', payload: {
            where: 'main',
            data: null
        }})
    }
});

export default connect(
    null,
    mapDispatchToProps
)(Page);
