import React from 'react';
import { connect } from 'react-redux';

class Harmonogram extends React.Component {
    render() {
        const { harmonogram, user } = this.props;
        return (
            <div>
                <div className="head clearfix">
                    <h3 className="pull-left">Harmonogram</h3>
                    {user.role === 'host' ? <button className="btn btn-success pull-right" onClick={this.props.addEvent}>Dodaj wydarzenie</button> : null}
                </div>
                <table className="table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Wydarzenie</th>
                            <th>Data</th>
                            <th>-</th>
                        </tr>
                    </thead>
                    <tbody>
                        {harmonogram.map((e, i) =>
                            <tr key={e.id}>
                                <td>{i + 1}</td>
                                <td>{e.title}</td>
                                <td>{e.date}</td>
                                <td>
                                    {user.role === 'host' ? <button className="btn btn-primary" onClick={() => this.props.back(e)}>Edytuj</button> : null}
                                </td>
                            </tr>)}
                        </tbody>
                    </table>
            </div>
        )
    }
}

const mapDispatchToProps = (dispatch) => ({
    addEvent: () => {
        dispatch({type: 'CHANGE_DISPLAY', payload: {where: 'edit'}});
    },
    back: (data) => {
        dispatch({type: 'CHANGE_DISPLAY', payload: {where: 'edit', data}});
    }
});

export default connect(
    null,
    mapDispatchToProps
)(Harmonogram);
