import React from 'react';
import { connect } from 'react-redux';

class EventForm extends React.Component {
    constructor(props) {
        super(props);

        console.log(props);

        this.state = {
            id: props.id,
            title: props.title ? props.title : '',
            date: props.date ? props.date : ''
        };

        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit (el) {
        el.preventDefault();
        this.props.addEditEvent(this.state);
        this.setState({
            id: '',
            title: '',
            date: ''
        });
        this.props.back();
    }

    render() {
        return (
            <div>
                <form onSubmit={this.handleSubmit}>
                    <div className="form-group">
                        <label htmlFor="title">Nazwa wydarzenia</label>
                        <input type="text" className="form-control" id="title" placeholder="Nazwa wydarzenia" value={this.state.title} onChange={(e) => this.setState({title: e.target.value})} />
                    </div>
                    <div className="form-group">
                        <label htmlFor="date">Data wydarzenia</label>
                        <input type="text" className="form-control" id="date" placeholder="Data wydarzenia" value={this.state.date} onChange={(e) => this.setState({date: e.target.value})} />
                    </div>
                    <button type="submit" className="btn btn-primary">Dodaj</button>
                    <button className="btn btn-danger" onClick={this.props.back}>Anuluj</button>
                </form>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    id: state.main.display.data ? state.main.display.data.id : +(Math.random()*1000000).toFixed(0),
    title: state.main.display.data ? state.main.display.data.title : '',
    date: state.main.display.data ? state.main.display.data.date : ''
});

const mapDispatchToProps = (dispatch) => ({
    back: () => {
        dispatch({type: 'CHANGE_DISPLAY', payload: {where: 'main', data: null}});
    },
    addEditEvent: (payload) => {
        dispatch({type: 'ADD_EDIT_EVENT', payload});
    }
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(EventForm);
